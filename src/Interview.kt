import java.util.*

class Interview(val candidate: Candidate, var position: Position, val interviewer: Interviewer) {
    val id = UUID.randomUUID().toString()
    var status: InterviewStatus = InterviewStatus.UPCOMING

    fun finish() {
        this.status = InterviewStatus.COMPLETED
    }
}

enum class InterviewStatus {
    UPCOMING,
    COMPLETED
}