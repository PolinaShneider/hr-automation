import java.util.*

class Application(info: Candidate, var position: Position) {
    val id = UUID.randomUUID().toString()
    var candidate: Candidate = info
    var status: Status = Status.PENDING
}