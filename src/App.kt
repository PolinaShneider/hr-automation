fun main() {
    val position = Position(requirements = listOf(
        Requirement(Technology.HTML, 3),
        Requirement(Technology.HASKEL, 2)
    ), team = Team())

    val candidate = Candidate(
        experience = Experience(
            listOf(
                Requirement(Technology.HTML, 3),
                Requirement(Technology.HASKEL, 2),
                Requirement(Technology.CPP, 2)
            )
        ),
        fullName = "Pedro Lukas"
    )

    val recruiter = HR("Sarah Bell")
    val interviewer = Interviewer("Pete Berkley")
    candidate.browse(position)
}