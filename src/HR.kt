class HR(fullName: String) : User(fullName) {
    private val repository = Repository.getInstance()

    init {
        repository.applications.subscribe(this)
    }

    fun notifyAbout(id: String) {
        val application = repository.getElementById(DataType.APPLICATION, id)
        val interviewer = repository.interviewers.getPerson()

        if (application != null) repository.interviews.add(
            Interview((application as Application).candidate, application.position, interviewer)
        )
    }
}
