package data

import Application
import HR

class Applications() {
    private var applications = ArrayList<Application>()
    private var subscribers = ArrayList<HR>()

    fun subscribe(subscriber: HR) {
        subscribers.add(subscriber)
    }

    fun add(application: Application) {
        applications.add(application)
        subscribers.forEach{
            it.notifyAbout(application.id)
        }
    }

    fun getData(): ArrayList<Application> {
        return this.applications
    }
}