package data

import Interviewer
import kotlin.collections.ArrayList
import kotlin.random.Random

class Interviewers {
    private var persons = ArrayList<Interviewer>()

    fun add(person: Interviewer) {
        persons.add(person)
    }

    fun getPerson(): Interviewer {
        val index = Random.nextInt(this.persons.size)
        return this.persons[index]
    }
}