package data

import Interview
import Interviewer

class Interviews() {
    private var interviews = ArrayList<Interview>()
    private var subscribers = ArrayList<Interviewer>()

    fun subscribe(subscriber: Interviewer) {
        subscribers.add(subscriber)
    }

    fun add(interview: Interview) {
        interviews.add(interview)
        subscribers.forEach{
            it.notifyAbout(interview.id)
        }
    }

    fun getData(): ArrayList<Interview> {
        return this.interviews
    }
}