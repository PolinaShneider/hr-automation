class Experience(skills: List<Requirement>) {
    val technologies: List<Requirement> = skills

    fun compare(requirements: List<Requirement>): Boolean {
        requirements.forEach {
            val target = this.technologies.find{ that ->
                that.technology == it.technology
            }

            if (target == null || target.years < it.years) {
                return false
            }
        }

        return true
    }
}

data class Requirement(
    val technology: Technology,
    val years: Int
)

enum class Technology {
    HTML,
    CSS,
    REACT,
    VUE,
    JAVA,
    KOTLIN,
    CPP,
    C_SHARP,
    RUBY,
    HASKEL
}
