class Interviewer(fullName: String): User(fullName) {
    private val repository = Repository.getInstance()

    init {
        repository.interviewers.add(this)
        repository.interviews.subscribe(this)
    }

    fun notifyAbout(id: String) {
        print("You have an interview, come on")
    }
}
