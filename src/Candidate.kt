import data.Applications

class Candidate(fullName: String, val experience: Experience): User(fullName) {
    private val repository = Repository.getInstance()

    private fun apply(position: Position) {
        repository.applications.add(Application(this, position))
    }

    fun browse(position: Position) {
        if (this.experience.compare(position.requirements)) {
            apply(position)
        }
    }
}