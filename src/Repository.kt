import data.Applications
import data.Interviewers
import data.Interviews

class Repository {
    val applications: Applications = Applications()
    val interviews: Interviews = Interviews()
    val interviewers: Interviewers = Interviewers()

    fun getElementById(type: DataType, id: String): Any? {
        return when (type) {
            DataType.APPLICATION -> this.applications.getData().find {
                it.id == id
            }
            DataType.INTERVIEW -> this.interviews.getData().find {
                it.id == id
            }
            else -> null
        }
    }

    companion object {
        private var obj: Repository? = null
        fun getInstance(): Repository {
            if (obj == null) {
                obj = Repository()
            }
            return obj as Repository
        }
    }
}

enum class DataType {
    APPLICATION,
    INTERVIEWER,
    INTERVIEW
}